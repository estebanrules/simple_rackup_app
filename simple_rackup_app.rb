# Exercise:  Previously you have written a simple Rack application which when run, accepts a
# command-line argument and displays this command-line argument.
# Re-write this app using rackup and config.ru

class MyRackApp
  def call(env)
    argument = "Hello Earth"
    [200, {"Content-Type" => "text/html"}, ["Command line argument you typed was: #{argument}"]]
  end
end
